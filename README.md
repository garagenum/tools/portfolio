# portfolio

Les portfolios montrent vos talents créatifs ou professionnels d'une manière beaucoup plus vaste et complexe qu'un CV. Les éléments à inclure dans un portfolio dépendent en grande partie du domaine d'expertise.

On crée un portfolio (ou portefeuil de compétences numérique) le plus souvent sur des plateformes de création en ligne tels que: "Behance", "Journoportfolio", "Wix" etc...
Elles sont souvent gratuites, proposent un large choix de designs, hébergent les oeuvres des utilisateurs, font parfois office de site d'annonce ou de réseaux sociaux, et sont relativement simples d'utilisation.

On a la possiblité d'utiliser des CMS open source tels que:

- **Wordpress** qui grace à des thèmes qui proposent d'avance une mise en page avec différentes sections prédéfinies, à des plugins gratuits comme:

  - **nimble portfolio** qui vont nous permettre de mettre en place un filtrage des catégories et qui s'integrera dirrectement au thème choisi préalablement.
  (lien vers tutoriel de config: https://www.youtube.com/watch?v=diEzT72EtwA)

  - **Elementor** qui permet de créer facilement des pages à structures complexes tres simplement etc...
  (lien vers tutoriel: https://www.youtube.com/watch?v=ctpCTo-Fc9Y)

- **Karuta**, est une application web de construction et de gestion de portfolios inventée dans le cadre d’un programme de recherche canadien à *HEC Montréal*, portée initialement par la *MATI*, un centre de recherche des technologies en formation et apprentissage puis par la *chaire* en apprentissage dirigés par **Jacques RAYNAULD**, afin de répondre à des problématiques pédagogiques universitaires et des besoins très divers en termes de démarches portfolio intégrées. (lien vers le site: https://karuta-france-portfolio.fr/).

- **Drupal** est un Outil de choix pour faire tourner les gros sites au trafic important, il permet de faire tout ce qu'on veut. Très complet, il est aussi très sécurisé. Par contre, il est difficile d’accès et de grandes connaissances html seront nécessaires pour le gérer.Des dizaines de milliers de personnes et d'organisations utilisent Drupal pour publier des sites de toutes tailles et fonctions.



- Nous avons finalement choisi comme Outil **Hugo** qui se targue d'être je site "*Le cadre le plus rapide au monde pour la création de sites Web*".
**Hugo** est un logiciel Open Source qui a été créé en 2013 et qui est hébergé sur la plateforme de développement *Github*. La première particularité de **Hugo** vient de son mode de développement. Il utilise un langage de programmation assez récent appelé **Golang** et créé par *Google*.

Lorsque le logiciel est utilisé, il crée un ensemble de fichiers HTML et il n'y a plus qu’à déposer ces fichiers sur l’hébergement choisi. Contrairement à un CMS comme **Wordpress**, il n'est pas possible de proposer des interactions avec les utilisateurs d’un site basé sur Hugo (comme des commentaires par exemple) directement. Cependant, le site sera plus securisé et et l'hebergement sera moins couteux puisqu'on a plus besoin de *PHP*, *MySQL*, etc...


Le portfolio contiendra une présentation dans notre cas le nom de la formation , une brève explication des domaines dans lesquels elle opère, un shéma du déroulement du parcours et des échantillons de projets developpés pendant celle ci.
